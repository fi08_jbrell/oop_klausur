/**
 * @author J.Brell
 * @version 0.1.0
 * @since 21.01.23
 */
public class Composition {
    private final PolymorphParent generalGreeter;
    private final PolymorphChild specificGreeter;

    Composition(PolymorphParent generalGreeter, PolymorphChild specificGreeter) {
        this.generalGreeter = generalGreeter;
        this.specificGreeter = specificGreeter;
    }

    void greet(String name, boolean hasGoodMood) {
        if (hasGoodMood) {
            specificGreeter.greet();
        } else {
            if (name == null) {
                generalGreeter.greet();
            } else {
                generalGreeter.greet(name);
            }
        }
    }
}
