/**
 * @author J.Brell
 * @version 0.1.0
 * @since 20.01.23
 */
public class PolymorphParent {
    public void greet(String name) {
        System.out.println("Hello " + name);
    }

    public void greet() {
        System.out.println("Hello world");
    }
}
