import java.util.Arrays;
import java.util.List;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 20.01.23
 */
public class Generic <T> {

    private final T first;
    private final T second;

    Generic(T first, T second) {
        this.first = first;
        this.second = second;
    }

    List<T> getBoth() {
        return Arrays.asList(first, second);
    }
}
