### 1. Was bedeutet OOP. Grenze diesen Begriff zu anderen Programmierkonzepten ab? (Funktional, Imperativ).

OOP bedeutet Object Oriented Programming. Daten und dazugehörige Funktionen werden zu Objekten zusammengefasst.
Bei der funktionalen Programmierung werden Daten und Funktionen strikt getrennt. Eine Funktion hat Eingabe Parameter
und einen Rückgabewert und bleibt vom Status der Daten unberührt.
Beim imperativen Programmieren wird eine Abfolge von Befehlen ausgeführt.

### 2. Was ist der Unterschied zwischen einer Klasse, einem Objekt und einer Instanz?

Ein Objekt ist die Instanz einer bestimmten Klasse. Alle Instanzen einer Klasse haben das gleiche Verhalten, aber nicht
zwangsweise denselben Status.

### 3. Was bedeutet Datenkapselung?

Die Daten eines Objektes sind von außen weder einsehbar noch modifizierbar.

### 4. Beschreibe wie Datenkapselung in einer Programmiersprache funktioniert (Java, C###, PHP)

Felder und Methoden einer Klasse, die nicht zur API gehören, werden mit dem Modifier 'private' versehen, wenn sie
ausschließlich in dieser Klasse sichtbar sein sollen, bzw. mit 'protected', wenn sie zusätzlich in Unterklassen
sichtbar sein sollen.

### 5. Erläutere Vererbung: Interfacevererbung, Implementierungsvererbung

Bei Vererbung gibt eine Elternklasse ihr Verhalten an eine Unterklasse weiter. Bei der Interfacevererbung ist lediglich
das Verhalten gleich, bei der Implementierungsvererbung auch die Implementierung.

### 6. Was beschreibt das Liskov‘sche Substitutionsprinzip?

Alle Klassen, die von einer Elternklasse erben, müssen das gleiche Verhalten aufweisen wie ihre Elternklasse.
Überall, wo eine Elternklasse 'A' gefordert ist, kann ich eine Kindklasse von 'A' substituieren.

### 7. Gib ein Beispiel, wo dieses Prinzip verletzt ist (Kein Pinguin)

Eine Elternklasse 'List' ist eine Collection mit einer 'Add'-Methode. Eine Kindklasse ist 'UnmutableList',
die aufgrund dessen, dass sie nicht mutierbar ist, über keine 'Add' Methode verfügt.

### 8. Was ist Polymorphismus?

Polymorphismus ist das Überschreiben und Überladen von Methoden desselben Namens.

### 9. Unterscheide statische, dynamische und Ad-Hoc Polymorphie.

Bei der statischen Polymorphie existieren mehrere Implementierungen einer Methode innerhalb der gleichen Klasse.
Welche Implementierung der Methode aufgerufen wird, entscheidet sich durch deren Parameter. Dies kann durch statische
Typen bereits zur Kompilierzeit ermittelt werden. Dies wird Überladen genannt.

↑ Oder ist das Ad-Hoc? ↑

Bei der dynamischen Polymorphie überschreibt eine Kindklasse die Implementierung einer von der Elternklasse geerbten
Methode. Die Parameter der überschriebenen Methode sind dabei identisch mit der überschriebenen Methode. Welche
Implementierung einer Methode aufgerufen wird, ermittelt sich dynamisch während der Laufzeit über eine virtuelle
Methodentabelle.

### 10. Wie sehen diese beiden Polymorphie-Typen bei Java/PHP aus? Programmiere dazu Beispiele.

Siehe [PolymorphParent](src/PolymorphParent.java) und [PolymorphChild](src/PolymorphChild.java).

### 11. Erläutere die Begriffe Assoziation, Aggregation und Komposition

Sind Klassen sind assoziiert, wenn ein Objekt einer Klasse eine Referenz zu einem Objekt einer anderen Klasse hat.

Bei der Aggregation und Komposition handelt es sich um eine Teil/Ganzes Beziehung. Bei der Aggregation können die
einzelnen Teile unabhängig von ihrer aggregierenden Klasse existieren. Die Menge der aggregierten Teile ist beliebig
und kann daher auch null sein.

Bei einer Komposition können die einzelnen Teile nur existieren, wenn sie zu einem Ganzen gehören und umgekehrt muss
das Ganze aus mindestens einem Teil bestehen.

### 12. Was sind Generics bzw. Templates bzw. generische Programmierung?

Eine generische Klasse verfügt über Typenparameter. So kann die gleiche Implementierung einer Klasse für verschiedene
Typen verwendet werden. Bei Instanziierung eines Objekts muss der generische Typ definiert werden.

### 13. Programmiere hierzu ein Beispiel in Java/PHP.

Siehe [Generic](src/Generic.java).

### 14. Was bedeutet: "Favour Composition over Inheritance"

Anstelle ein gewünschtes Verhalten zu vererben, wird stattdessen ein Objekt mit dem gewünschten Verhalten referenziert.

### 15. Programmiere hierzu ein Beispiel in Java/PHP.

Siehe [Composition](src/Composition.java)

### 16. Was bedeutet das „S“ in den Solid-Prinzipen. Gib ein Beispiel an, in der das Prinzip verletzt wird.

Das 'S' steht für SRP - Single Responsibility Principle.

Wenn eine Klasse "Book" über eine Methode "printCurrentPage()" verfügt, ist das Prinzip verletzt.
Das Buch Objekt sollte lediglich die Information zurückgeben, welche Seite momentan aufgeschlagenen ist.
Die Methode "printPage()" sollte eher in der "Page" Klasse verortet sein oder besser noch in einer gesonderten "Printer"
Klasse.

### 17. Was bedeutet das „O“ in den Solid-Prinzipen. Gib ein Beispiel an, in der das Prinzip eingesetzt wird.

Das 'O' steht für OCP - Open-Closed-Prinzip.

Module sollten offen für Erweiterungen und verschlossen für Modifikationen sein.

Bei Vererbung wird die bestehende Basisklasse nicht modifiziert, sondern durch die Funktionalität ihrer Kindklassen
erweitert.

### 18. Was bedeutet das „D“ in den Solid-Prinzipien.

Das 'D' steht für Dependency-Inversion-Principle.

Module höherer Ordnung sollten nicht von Modulen niedrigerer Ordnung abhängen. Abstraktionen sollten nicht von Details
abhängen, sondern umgekehrt.
